// bin2sha project main.go
package main

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

func hash_file_sha256(filePath string) (string, error) {
	//Initialize variable returnSHAString now in case an error has to be returned
	var returnSHAString string

	//Open the passed argument and check for any error
	file, err := os.Open(filePath)
	if err != nil {
		return returnSHAString, err
	}

	//Tell the program to call the following function when the current function returns
	defer file.Close()

	//Open a new hash interface to write to
	hash := sha256.New()

	//Copy the file in the hash interface and check for any error
	if _, err := io.Copy(hash, file); err != nil {
		return returnSHAString, err
	}

	//Get the 16 bytes hash
	hashInBytes := hash.Sum(nil)[:16]

	//Convert the bytes to a string
	returnSHAString = hex.EncodeToString(hashInBytes)

	return returnSHAString, nil

}

func main() {

	//Folder Path
	searchDir := os.Args[1]

	fileList := []string{}

	//Get files in the folder
	err := filepath.Walk(searchDir, func(path string, f os.FileInfo, err error) error {
		fileList = append(fileList, path)
		return nil
	})

	if err == nil {
		//for each file
		for _, file := range fileList {
			//get Hash
			hash, err := hash_file_sha256(file)
			if err == nil {
				fmt.Println(file + " -> " + hash)
			}
		}
	} else {
		fmt.Println(err)
	}

}

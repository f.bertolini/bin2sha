## Bin2Sha ##

Simple application wrote in GoLang to create SHA256 hashes of all the files 
in a directory recursively.

``` ./bin2sha /path/to/files/ > output.txt ```